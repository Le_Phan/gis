$(document).ready(function() {
	//set panel size
    function setHeight() {
        var header =  $('#gis-header');
        var body = $('#gis-body');
        var sidebar = $('.sidebar-scroll-bar');
        var subMenu = $('#sub-menu-nav');
        var map = $('#gis-map');
        var h = document.body.clientHeight - header.height();
        var w = document.body.clientWidth - 260;
        body.height(h);
        sidebar.height(h);
        map.height(h);
        map.width(w);
        sidebar.height(h-40);
    };
    setHeight();
    //set scrollbar when window resize
    var resizeTimer;
    $(window).resize(function() {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(setHeight, 100);
    });
    //add checktree
    $('.checktree').checktree();
	//treeview-menu
    $('.treeview').on('click', function (e) {
        e.preventDefault();
        if ($(this).parent().hasClass('active')) {
            $(this).parent().removeClass('active');
        }
        else {
            $(this).parent().addClass('active');
        }
    });

    //visible/hidden tabs
    $('#setting-tab-1').on('click', function (e) {
        e.preventDefault();
        if ($('#tab-1').hasClass('g-hidden')) {
            $('#tab-1').removeClass('g-hidden');
        }
        else {
            $('#tab-1').addClass('g-hidden');
        }
    });
    $('#setting-tab-2').on('click', function (e) {
        e.preventDefault();
        if ($('#tab-2').hasClass('g-hidden')) {
            $('#tab-2').removeClass('g-hidden');
        }
        else {
            $('#tab-2').addClass('g-hidden');
        }
    });
    $('#setting-tab-3').on('click', function (e) {
        e.preventDefault();
        if ($('#tab-3').hasClass('g-hidden')) {
            $('#tab-3').removeClass('g-hidden');
        }
        else {
            $('#tab-3').addClass('g-hidden');
        }
    });
    $('#setting-tab-4').on('click', function (e) {
        e.preventDefault();
        if ($('#tab-4').hasClass('g-hidden')) {
            $('#tab-4').removeClass('g-hidden');
        }
        else {
            $('#tab-4').addClass('g-hidden');
        }
    });
    $('#setting-tab-5').on('click', function (e) {
        e.preventDefault();
        if ($('#tab-5').hasClass('g-hidden')) {
            $('#tab-5').removeClass('g-hidden');
        }
        else {
            $('#tab-5').addClass('g-hidden');
        }
    });
    $('#setting-tab-6').on('click', function (e) {
        e.preventDefault();
        if ($('#tab-6').hasClass('g-hidden')) {
            $('#tab-6').removeClass('g-hidden');
        }
        else {
            $('#tab-6').addClass('g-hidden');
        }
    });

    //visible/hidden maps
    $('#setting-google-maps').on('click', function (e) {
        e.preventDefault();
        if ($('#google-maps').hasClass('g-hidden')) {
            $('#google-maps').removeClass('g-hidden');
        }
        else {
            $('#google-maps').addClass('g-hidden');
        }
    });
    $('#setting-google-satelite').on('click', function (e) {
        e.preventDefault();
        if ($('#google-satelite').hasClass('g-hidden')) {
            $('#google-satelite').removeClass('g-hidden');
        }
        else {
            $('#google-satelite').addClass('g-hidden');
        }
    });
    //visible/hidden Xã
    $('#setting-g-ward').on('click', function (e) {
        e.preventDefault();
        if ($('#g-ward').hasClass('g-hidden')) {
            $('#g-ward').removeClass('g-hidden');
        }
        else {
            $('#g-ward').addClass('g-hidden');
        }
    });

    //chanel
    $(function() {
        //visible/hidden Kênh phân phối
        $('.chanel-group').hide();
        $('#chanel-1').show();//kênh mặc định
        $('#setting-chanel-1').on('click', function (e) {
            e.preventDefault();
            $('.chanel-group').hide();
            $('#chanel-1').show();
            $('.chanel').show();
        });
        //Kênh phân phối theo khối lượng
        $('#setting-chanel-2').on('click', function (e) {
            e.preventDefault();
            $('.chanel-group').hide();
            $('#chanel-2').show();
        });
        //visible/hidden Kênh phân phối theo từng vùng miền
        //default
        //chanel-1-1
        $('#setting-chanel-1-1').on('click', function (e) {
            e.preventDefault();
            $('.chanel-group').hide();
            $('#chanel-1').show();
            $('.chanel').hide();
            $('#chanel-1-1').show();
        });
        //chanel-1-2
        $('#setting-chanel-1-2').on('click', function (e) {
            e.preventDefault();
            $('.chanel-group').hide();
            $('#chanel-1').show();
            $('.chanel').hide();
            $('#chanel-1-2').show();
        });
        //chanel-1-3
        $('#setting-chanel-1-3').on('click', function (e) {
            e.preventDefault();
            $('.chanel-group').hide();
            $('#chanel-1').show();
            $('.chanel').hide();
            $('#chanel-1-3').show();
        });
        //chanel-1-4
        $('#setting-chanel-1-4').on('click', function (e) {
            e.preventDefault();
            $('.chanel-group').hide();
            $('#chanel-1').show();
            $('.chanel').hide();
            $('#chanel-1-4').show();
        });
    });

    //visible/hidden doanh-nghiep
    $('#company-2').addClass('g-hidden');
    $('#setting-company').on('click', function (e) {
        e.preventDefault();
        if ($('#company-1').hasClass('g-hidden')) {
            $('#company-1').removeClass('g-hidden');
            $('#company-2').addClass('g-hidden');
        }
        else {
            $('#company-2').removeClass('g-hidden');
            $('#company-1').addClass('g-hidden');
        }
    });
    
    //visible/hidden Kho
    $('#storehouse-2').addClass('g-hidden');
    $('#setting-storehouse').on('click', function (e) {
        e.preventDefault();
        if ($('#storehouse-1').hasClass('g-hidden')) {
            $('#storehouse-1').removeClass('g-hidden');
            $('#storehouse-2').addClass('g-hidden');
        }
        else {
            $('#storehouse-2').removeClass('g-hidden');
            $('#storehouse-1').addClass('g-hidden');
        }
    });

    //visible/hidden Nhà máy
    $('#factory-2').addClass('g-hidden');
    $('#setting-factory').on('click', function (e) {
        e.preventDefault();
        if ($('#factory-1').hasClass('g-hidden')) {
            $('#factory-1').removeClass('g-hidden');
            $('#factory-2').addClass('g-hidden');
        }
        else {
            $('#factory-2').removeClass('g-hidden');
            $('#factory-1').addClass('g-hidden');
        }
    });


    //visible/hidden Doanh nghiệp vận tải
    $('#transport-enterprise-2').addClass('g-hidden');
    $('#setting-transport-enterprise').on('click', function (e) {
        e.preventDefault();
        if ($('#transport-enterprise-1').hasClass('g-hidden')) {
            $('#transport-enterprise-1').removeClass('g-hidden');
            $('#transport-enterprise-2').addClass('g-hidden');
        }
        else {
            $('#transport-enterprise-2').removeClass('g-hidden');
            $('#transport-enterprise-1').addClass('g-hidden');
        }
    });

    $(function() {
        //visible/hidden Tỉnh
        $('.province').hide();
        $('#province-1').show();
        $('#setting-province-1').on('click', function (e) {
            e.preventDefault();
            $('.province-group').show();
            $('.province').hide();
            $('#province-1').show();
        });
        $('#setting-province-2').on('click', function (e) {
            e.preventDefault();
            $('.province').hide();
            $('#province-2').show();
        });
        $('#setting-province-3').on('click', function (e) {
            e.preventDefault();
            $('.province').hide();
            $('#province-3').show();
        });
        //Tỉnh theo miền
        //default
        //province-1-1
        $('#setting-province-1-1').on('click', function (e) {
            e.preventDefault();
            $('.province').hide();
            $('#province-1').show();
            $('.province-group').hide();
            $('#province-1-1').show();
        });
        //province-1-2
        $('#setting-province-1-2').on('click', function (e) {
            e.preventDefault();
            $('.province').hide();
            $('#province-1').show();
            $('.province-group').hide();
            $('#province-1-2').show();
        });
        //province-1-3
        $('#setting-province-1-3').on('click', function (e) {
            e.preventDefault();
            $('.province').hide();
            $('#province-1').show();
            $('.province-group').hide();
            $('#province-1-3').show();
        });
        //province-1-4
        $('#setting-province-1-4').on('click', function (e) {
            e.preventDefault();
            $('.province').hide();
            $('#province-1').show();
            $('.province-group').hide();
            $('#province-1-4').show();
        });
    });

    //visible/hidden Huyen
    $('#district-2').addClass('g-hidden');
    $('#district-3').addClass('g-hidden');
    $('#setting-district-1').on('click', function (e) {
        e.preventDefault();
        $('.district').addClass('g-hidden');
        $('#district-1').removeClass('g-hidden');
    });
    $('#setting-district-2').on('click', function (e) {
        e.preventDefault();
        $('.district').addClass('g-hidden');
        $('#district-2').removeClass('g-hidden');
    });
    $('#setting-district-3').on('click', function (e) {
        e.preventDefault();
        $('.district').addClass('g-hidden');
        $('#district-3').removeClass('g-hidden');
    });

});