jQuery(function(){
  jQuery(".character-limit").each(function(i){
	var	len=jQuery(this).text().length;
	var limit = jQuery(this).attr("data-limit");
    if(len>limit)
    {
      jQuery(this).text($(this).text().substr(0,limit)+'...');
    }
  });
  jQuery(".word-limit").each(function(i){
	var	text	=	jQuery(this).text().split(" ");
	var limit = jQuery(this).attr("data-limit");
    if(text.length >limit)
    {
		var result = "";
		for(i=0;i<limit;i++) result+=text[i]+ " "; 
			
      jQuery(this).text(result+'...');
    }
  });
});